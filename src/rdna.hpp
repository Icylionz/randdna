#include <string>
#include <random>
#include <iostream>
using std::string;
using namespace std;
string randDNA(int seed, string bases, int n)
{
	mt19937 eng(seed);
	int rIndex;
	char dnaPiece;
	int min=0,max=bases.length()-1;
	int i;
	string dna="";
	uniform_int_distribution<> unifrm(min,max);
	for(i=0;i<n;i++)
	{
		rIndex=unifrm(eng);
		dnaPiece=bases[rIndex];
		dna= dna + dnaPiece;
	}
	return dna;
}
